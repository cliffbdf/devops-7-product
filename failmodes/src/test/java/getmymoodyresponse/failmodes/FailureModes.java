package getmymoodyresponse.failmodes;

import hello.stub.HelloStub;
import hello.stub.HelloFactory;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class FailureModes {

	HelloStub hello = null;
	String clusterName = null;
	Throwable requestThrowable = null;

	public FailureModes() throws Exception {

		// Create stub for accessing the Hello service:
		String helloFactoryClassName = System.getenv("HELLO_FACTORY_CLASS_NAME");
		if (helloFactoryClassName == null) throw new Exception(
			"Env variable HELLO_FACTORY_CLASS_NAME is not defined");
		HelloFactory helloFactory = (HelloFactory)
			(Class.forName(helloFactoryClassName).getDeclaredConstructor().newInstance());
		this.hello = helloFactory.createHello();
	}

	@Given("the Hello and WeGood services are deployed in cluster {string} and available")
	public void hello_and_wegood_are_available(String clusterName) throws Exception {
		this.clusterName = clusterName;
		String response = null;

		for (int i = 0; i < 3; i++) {
			try { response = hello.hello(); } catch (Exception ex) {
				new Timer(1000, () -> {});  // wait one second
				continue; // try again
			}
			break;
		}

		assertTrue(response.equals("Hello world!") || response.equals("Goodbye"));
	}

	Timer timer = null;
	@Given("WeGood becomes unavailable for {int} seconds")
	public void wegood_becomes_unavailable_for(int seconds) throws Exception {
		/* Inject failure: stop the WeGood service for up to 2 seconds, or after
			the next test step completes - which ever completes first. */
		String serviceName = "wegood";
		updateDesiredCount(this.clusterName, serviceName, 0);
		this.timer = new Timer(2000,  // sleep for up to 2 seconds
			() -> {  // do this if it is interrupted before the time is up
				try {
					updateDesiredCount(clusterName, serviceName, 2);
				} catch (Exception ex) { ex.printStackTrace(); }
			}
		);
	}

	String response = null;
	@When("I make a request to Hello")
	public void make_request_to_hello() throws Exception {
		try { response = hello.hello(); }
		catch (Throwable t) {
			this.requestThrowable = t;
		}
	}

	@Then("I receive a runtime exception response from {string} indicating a server problem")
	public void runtime_exception_response(String serviceName) throws Exception {
		try {
			assertNotNull(this.requestThrowable);
		} finally {
			if (timer.isWaiting()) {
				this.timer.cancel();
				// Restore the instance count:
				updateDesiredCount(this.clusterName, serviceName, 2);
			}
		}
	}

	/**
	Update the desired instance count for the specified service in the specified
	AWS ECS cluster.
	*/
	private static void updateDesiredCount(String clusterName, String serviceName,
		int count) throws Exception {

		String awsAccKeyId = System.getenv("AWS_ACCESS_KEY_ID");
		String awsSecAccKey = System.getenv("AWS_SECRET_ACCESS_KEY");
		if (awsAccKeyId == null) throw new Exception("AWS_ACCESS_KEY_ID not set");
		if (awsSecAccKey == null) throw new Exception("AWS_SECRET_ACCESS_KEY not set");

		performShellCommand(
			"aws", "ecs", "update-service", "--desired-count", String.valueOf(count),
			"--cluster", clusterName, "--service", serviceName);

		System.err.println("Set desired count for " + serviceName + " to " + count);
	}

	/**
	Perform the specified shell command in an operating system shell, and block until
	the process completes. If the command takes longer than 15 seconds, abort it and
	throw an exception.
	*/
	private static void performShellCommand(String... commandParts) throws Exception {

		ProcessBuilder pb = new ProcessBuilder(commandParts);
		pb.redirectOutput(pb.redirectError());

		System.err.println("Performing shell command:");
		for (String cmdpart : commandParts) System.err.print(cmdpart + " ");
		System.err.println();
		Process process = pb.start();

		long ms = 15000;
		boolean finished = process.waitFor(ms, TimeUnit.MILLISECONDS);
		if (finished) {
			System.err.println("Command completed");
		} else {
			System.err.println("Command did not complete");
			try { process.destroy(); }
			finally {
				throw new Exception("Command did not complete in " + ms + "ms");
			}
		}

		int exitValue = process.exitValue();
		// Ref: https://docs.aws.amazon.com/cli/latest/topic/return-codes.html
		if (exitValue != 0) throw new Exception("Command exited with status " + exitValue);
	}

	/**
	When an instance of Timer is created, it waits for the specified time. If it is
	interrupted because cancel was called, it performs the doIfInterrupted method.
	*/
	private class Timer {

		Thread t = new Thread();
		boolean isSleeping = false;

		Timer(int milliseconds, Runnable doIfInterrupted) {
			try {
				t.sleep(milliseconds);
				isSleeping = true;
				t.join();
			}
			catch (InterruptedException ex) { doIfInterrupted.run(); }
		}

		synchronized void cancel() {
			try {
				t.interrupt();
			} finally {
				isSleeping = false;
			}
		}

		synchronized boolean isWaiting() { return this.isSleeping; }
	}
}
