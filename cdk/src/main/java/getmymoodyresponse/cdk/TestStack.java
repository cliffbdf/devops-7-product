package getmymoodyresponse.cdk;

import hello.cdk.Hello;
import wegood.cdk.WeGood;

import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.core.StackProps;
import software.amazon.awscdk.services.ec2.Vpc;
import software.amazon.awscdk.services.ec2.Port;
import software.amazon.awscdk.services.ec2.SecurityGroup;
import software.amazon.awscdk.services.ec2.ISecurityGroup;
import software.amazon.awscdk.services.ecs.Protocol;
import software.amazon.awscdk.services.ecs.Cluster;
import software.amazon.awscdk.services.ecs.ContainerDefinitionOptions;
import software.amazon.awscdk.services.ecs.PortMapping;
import software.amazon.awscdk.services.ecs.ContainerDefinitionOptions;
import software.amazon.awscdk.services.servicediscovery.PrivateDnsNamespace;
import software.amazon.awscdk.services.appmesh.Mesh;
import software.amazon.awscdk.services.appmesh.MeshFilterType;
import software.amazon.awscdk.services.appmesh.VirtualNode;
import software.amazon.awscdk.services.appmesh.VirtualNodeListener;
import software.amazon.awscdk.services.appmesh.VirtualService;
import software.amazon.awscdk.services.appmesh.IVirtualService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Based on CompleteStack */
public class TestStack extends Stack {
	public TestStack(final Construct scope, final String id) {
		this(scope, id, null);
	}

	public TestStack(final Construct scope, final String id, final StackProps props) {
		super(scope, id, props);

		/* ---------------------------------------------------------------------
		Create VPC for fargate cluster with 2 max AZ's
		*/
		Vpc vpc = Vpc.Builder.create(this, "moody-vpc")
			.maxAzs(2)  // Default is all AZs in region
			.build();

		/* ---------------------------------------------------------------------
		Set up private DNS name for auto service discovery
		*/
		PrivateDnsNamespace privateDnsNamespace =
			PrivateDnsNamespace.Builder.create(this, "moody.local")
			.name("moody.local") .vpc(vpc) .build();

		/* ---------------------------------------------------------------------
		Create ECS fargate cluster within VPC created above
		*/
		Cluster cluster = Cluster.Builder.create(this, "cluster").clusterName("moody-cluster")
			.vpc(vpc).build();

		/* ---------------------------------------------------------------------
		Define the service mesh
		*/
		Mesh moodyMesh = Mesh.Builder.create(this, "moody-mesh")
			.meshName("moody-mesh")
			.egressFilter(MeshFilterType.ALLOW_ALL)
			.build();

		/* ---------------------------------------------------------------------
		Define the WeGood service
		*/

		WeGood wegood = new WeGood(this, cluster, "wegood", "wegood", privateDnsNamespace);

		VirtualNode virtualNode_wegood = VirtualNode.Builder.create(this, "wegood-vn")
			.mesh(moodyMesh)
			.virtualNodeName("wegood-vn")
			.dnsHostName("wegood.moody.local")
			.listener(VirtualNodeListener.builder()
				.portMapping(software.amazon.awscdk.services.appmesh.PortMapping.builder()
					.port(4567)
					.protocol(software.amazon.awscdk.services.appmesh.Protocol.HTTP)
					.build())
				.build())
			.build();

		VirtualService virtualService_wegood = VirtualService.Builder.create(this,"wegood-vs")
			.virtualServiceName("wegood.moody.local")
			.virtualNode(virtualNode_wegood)
			.mesh(moodyMesh)
			.build();

		/* ---------------------------------------------------------------------
		Define the Hello service
		*/

		Hello hello = new Hello(this, cluster, "hello", "hello",
			"wegood.stub.WeGoodProxyFactory", "http://wegood.moody.local:4567",
			privateDnsNamespace);

		java.util.List<IVirtualService> backendList = new ArrayList<IVirtualService>();
		backendList.add(virtualService_wegood);

		VirtualNode virtualNode_hello = VirtualNode.Builder.create(this, "hello-vn")
			.mesh(moodyMesh)
			.virtualNodeName("hello-vn")
			.dnsHostName("hello.moody.local")
			.listener(VirtualNodeListener.builder()
				.portMapping(software.amazon.awscdk.services.appmesh.PortMapping.builder()
					.port(4567)
					.protocol(software.amazon.awscdk.services.appmesh.Protocol.HTTP)
					.build())
				.build())
			.backends(backendList)
			.build();

		VirtualService virtualService_hello = VirtualService.Builder.create(this,"hello-vs")
			.virtualServiceName("hello.moody.local")
			.virtualNode(virtualNode_hello)
			.mesh(moodyMesh)
			.build();

		/* ---------------------------------------------------------------------
		Add allowed route to each security group, to access wegood from hello
		via service discovery
		*/

		List<ISecurityGroup> helloServiceSG =  hello.getALBFS()
			.getService().getConnections().getSecurityGroups();

		List<ISecurityGroup> wegoodServiceSG = wegood.getALBFS()
			.getService().getConnections().getSecurityGroups();

		wegoodServiceSG.forEach((wegoodSG) -> {
			helloServiceSG.forEach((helloSG) -> {
				ISecurityGroup helloSecurityGroup = SecurityGroup.fromSecurityGroupId(
					this, "helloSG", helloSG.getSecurityGroupId());

				/* Default is to not allow */
				wegoodSG.getConnections().allowFrom(helloSecurityGroup.getConnections(),
					Port.tcp(4567),
					"Hello to WeGood service discovery SG");
			});
		});
	}
}
