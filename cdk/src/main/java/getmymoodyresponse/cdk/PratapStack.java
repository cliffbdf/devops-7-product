package getmymoodyresponse.cdk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.Duration;
import software.amazon.awscdk.core.RemovalPolicy;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.core.StackProps;
import software.amazon.awscdk.services.ec2.*;
import software.amazon.awscdk.services.ecs.Protocol;
import software.amazon.awscdk.services.ecr.Repository;
import software.amazon.awscdk.services.ecs.*;
import software.amazon.awscdk.services.ecs.patterns.*;
import software.amazon.awscdk.services.elasticloadbalancingv2.ApplicationTargetGroup;
import software.amazon.awscdk.services.iam.AnyPrincipal;
import software.amazon.awscdk.services.iam.Effect;
import software.amazon.awscdk.services.iam.ManagedPolicy;
import software.amazon.awscdk.services.iam.PolicyStatement;
import software.amazon.awscdk.services.iam.Role;
import software.amazon.awscdk.services.iam.ServicePrincipal;
import software.amazon.awscdk.services.logs.LogGroup;
import software.amazon.awscdk.services.servicediscovery.PrivateDnsNamespace;
import software.amazon.awscdk.services.ssm.StringParameter;
import software.amazon.awscdk.services.appmesh.Mesh;
import software.amazon.awscdk.services.appmesh.MeshFilterType;
import software.amazon.awscdk.services.appmesh.VirtualNode;
import software.amazon.awscdk.services.appmesh.VirtualNodeListener;
import software.amazon.awscdk.services.appmesh.VirtualService;
import software.amazon.awscdk.services.cloudwatch.Metric;
import software.amazon.awscdk.services.applicationautoscaling.EnableScalingProps;
import software.amazon.awscdk.services.applicationautoscaling.TargetTrackingScalingPolicy;
import software.amazon.awscdk.services.appmesh.IVirtualService;

public class PratapStack extends Stack {
	public PratapStack(final Construct scope, final String id) {
		this(scope, id, null);
	}

	public PratapStack(final Construct scope, final String id, final StackProps props) {
		super(scope, id, props);

		/* Create VPC for fargate cluster with 2 max AZ's */
		Vpc vpc = Vpc.Builder.create(this, "moody-vpc")
			.maxAzs(2)  // Default is all AZs in region
			.build();

		/* Set up private DNS name for auto service discovery */
		PrivateDnsNamespace privateDnsNamespace =
			PrivateDnsNamespace.Builder.create(this, "moody.local")
			.name("moody.local") .vpc(vpc) .build();

		/* Create ECS fargate cluster within VPC created above */
		Cluster cluster = Cluster.Builder.create(this, "cluster").clusterName("moody-cluster")
			.vpc(vpc).build();

		/* Create a custom Cloudwatch Loggroup to use across all microservices */
		LogDriver logDriverWegood = LogDriver.awsLogs(AwsLogDriverProps.builder()
			.streamPrefix("wegoodrest")
			.logGroup(LogGroup.Builder.create(this, "wegoodlg").logGroupName("/aws/microservice/wegoodgroup").removalPolicy(RemovalPolicy.DESTROY).build())
			.build());

		/* ECS Task Execution policy statement
		ECS Fargate sgent sometimes makes calls to ECS API on our behalf and these
		policy statement’s will give access to ECS for downloading images from ECR if we
		need to (although we are not doing that here) and putting logs to cloud watch,
		X-Ray, etc.
		*/
		List<String> taskExecutionPolicy = Arrays.asList(
			"ecr:GetAuthorizationToken",
			"ecr:BatchCheckLayerAvailability",
			"ecr:GetDownloadUrlForLayer",
			"ecr:BatchGetImage",
			"logs:CreateLogStream",
			"logs:PutLogEvents"
			);

		/* AWS read only access to Amazon SSM policy statement */
		List<String> ssmPolicy = Arrays.asList(
			"ssm:Describe*",
			"ssm:Get*",
			"ssm:List*"
			);

		/* AWS X-Ray Daemon to relay raw trace segments policy statement */
		List<String> xrayDeamonPolicy = Arrays.asList(
			"xray:PutTraceSegments",
			"xray:PutTelemetryRecords",
			"xray:GetSamplingRules",
			"xray:GetSamplingTargets",
			"xray:GetSamplingStatisticSummaries"
			);

		/* Create a load-balanced Fargate service for the wegood microservice and make it public */
		ApplicationLoadBalancedFargateService applicationLoadBalancedFargateService_wegood =
			ApplicationLoadBalancedFargateService.Builder.create(this, "wegood")
			.serviceName("wegood")
			.cluster(cluster)           // Required
			.cpu(512)                   // Default is 256
			.desiredCount(2)            // Default is 1
			.cloudMapOptions(CloudMapOptions.builder() // auto discovery mode
				.cloudMapNamespace(privateDnsNamespace)
				.name("wegood")
				.build())
			.taskImageOptions(	// adding wegood microservice from ECR repo
				ApplicationLoadBalancedTaskImageOptions.builder()
				.image(ContainerImage.fromRegistry("cliffberg/wegood"))
				.containerPort(4567)
				.containerName("wegood")
				.logDriver(logDriverWegood)
				.build())
			.memoryLimitMiB(1024)       // Default is 512
			.publicLoadBalancer(true)   // Default is false
			.build();

		/* Set healthchecks for Target Group supporting Wegood service */

		ApplicationTargetGroup applicationWeGoodTargetGroup =
			applicationLoadBalancedFargateService_wegood.getTargetGroup();

		applicationWeGoodTargetGroup.configureHealthCheck(
			software.amazon.awscdk.services.elasticloadbalancingv2.HealthCheck.builder()
			//.port("8000")
			.port("4567")
			.path("/")
			.interval(Duration.seconds(120))
			.timeout(Duration.seconds(100))
			.build());

		/* Set Autoscaling property on a service with max of 3 contianers and minimum of 1 */

		FargateService fargateService_wegood =
			applicationLoadBalancedFargateService_wegood.getService();

		/* If these are not set, the minimum capacity will be zero, and the first request
		to the service will fail.
		*/
		ScalableTaskCount scalecount_wegood =
			fargateService_wegood.autoScaleTaskCount(EnableScalingProps.builder()
			.maxCapacity(3)
			.minCapacity(1)
			.build());

		scalecount_wegood.scaleOnRequestCount("scaling_wegood",
			RequestCountScalingProps.builder()
			.policyName("requestCount_wegood")
			.requestsPerTarget(10)
			.scaleInCooldown(Duration.seconds(300))
			.scaleOutCooldown(Duration.seconds(300))
			.targetGroup(applicationWeGoodTargetGroup)
			.build());

		/* Add x-ray deamon to the wegood task definition to capture analytics on a service */
		ContainerDefinition containerDef_wegood =
			applicationLoadBalancedFargateService_wegood.getTaskDefinition()
			.addContainer("xray-daemon", ContainerDefinitionOptions.builder()
				.image(ContainerImage.fromRegistry("amazon/aws-xray-daemon:1"))
				.logging(logDriverWegood)
				.build());

		/* Set the x-ray deamon container port and protocol to capture anaytics */
		containerDef_wegood.addPortMappings(PortMapping.builder()
			.containerPort(2000)
			.hostPort(2000)
			.protocol(Protocol.UDP).build());

		/* Add ECS Task Execution policy to task Definition execution role */
		// Do I need this?
		applicationLoadBalancedFargateService_wegood.getTaskDefinition()
			.addToExecutionRolePolicy(PolicyStatement.Builder.create()
				.actions(taskExecutionPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add AWS read only access to Amazon SSM policy to task Definition execution role */
		applicationLoadBalancedFargateService_wegood.getTaskDefinition()
			.addToExecutionRolePolicy(PolicyStatement.Builder.create()
				.actions(ssmPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add AWS X-Ray Daemon to relay raw trace segments data to the service's API and
		retrieve sampling data (rules, targets, etc.) to be used by the X-Ray SDK to
		Amazon SSM policy to task Definition execution role
		*/
		applicationLoadBalancedFargateService_wegood.getTaskDefinition()
			.addToExecutionRolePolicy(PolicyStatement.Builder.create()
				.actions(xrayDeamonPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add ECS Task Execution policy to task Definition execution role */
		// Do I need this?
		applicationLoadBalancedFargateService_wegood.getTaskDefinition()
			.addToTaskRolePolicy(PolicyStatement.Builder.create()
				.actions(taskExecutionPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add AWS read only access to Amazon SSM policy to task Definition execution role */
		applicationLoadBalancedFargateService_wegood.getTaskDefinition()
			.addToTaskRolePolicy(PolicyStatement.Builder.create()
				.actions(ssmPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add AWS X-Ray Daemon to relay raw trace segments data to the service's API and
		retrieve sampling data (rules, targets, etc.) to be used by the X-Ray SDK
		to Amazon SSM policy to task Definition execution role
		*/
		applicationLoadBalancedFargateService_wegood.getTaskDefinition()
			.addToTaskRolePolicy(PolicyStatement.Builder.create()
				.actions(xrayDeamonPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Set environment variables in SSM parameter store as secret as this will
		set valueFrom property in taskdefinition */

		String weGoodServiceURL = "http://wegood.moody.local:4567";
		String weGoodFactoryClassName = "wegood.stub.WeGoodProxyFactory";

		Map<String, Secret> env_hello_secret = new HashMap<String, Secret>();
		env_hello_secret.put("WEGOOD_SERVICE_URL", Secret.fromSsmParameter(
			StringParameter.Builder.create(this, "wegood_parameter_1")
				.parameterName("/dev/hello/WEGOOD_SERVICE_URL")
				.stringValue(weGoodServiceURL)
				.build()));
		env_hello_secret.put("WEGOOD_FACTORY_CLASS_NAME", Secret.fromSsmParameter(
			StringParameter.Builder.create(this, "wegood_parameter_2")
				.parameterName("/dev/hello/WEGOOD_FACTORY_CLASS_NAME")
				.stringValue(weGoodFactoryClassName)
				.build()));

		/* ---------------------Hello MicroService Settings--------------------- */

		/* LogDriver group for hello Microservice */
		LogDriver logDriverHello = LogDriver.awsLogs(AwsLogDriverProps.builder()
			.streamPrefix("hellorest")
			.logGroup(LogGroup.Builder.create(this, "helloLG")
				.logGroupName("hellogroup").removalPolicy(RemovalPolicy.DESTROY).build())
			.build());

		/* Configure the Fargate Hello AppMesh proxy */
		FargateTaskDefinition appMeshProxy_hello = FargateTaskDefinition.Builder.create(this, "proxy_hello")
			.proxyConfiguration(AppMeshProxyConfiguration.Builder.create()
				.containerName("envoy")
				.properties(AppMeshProxyConfigurationProps.builder()
					.appPorts(Arrays.asList(4567))
					//.appPorts(Arrays.asList(8100))
					.proxyIngressPort(15000)
					.proxyEgressPort(15001)
					//.egressIgnoredIPs(Arrays.asList("169.254.170.2", "169.254.169.254"))//169.254.170.2,169.254.169.254
					.ignoredUid(1337)
					.build())
				.build())
			.build();

		/* Add Hello container to the Hello taskdefinition */
		ContainerDefinition ContainerDefinition_hello = appMeshProxy_hello
			.addContainer("hello", ContainerDefinitionOptions.builder()
				.image(ContainerImage.fromRegistry("cliffberg/hello"))
				.secrets(env_hello_secret)
				.logging(logDriverHello)
				.build());

		/* Set Hello container ports.
		One must explicitly set ports when one creates those from CDK.
		*/
		// ....Do I need to do this for WeGood?
		ContainerDefinition_hello.addPortMappings(PortMapping.builder()
			.containerPort(4567)
			.hostPort(4567)
			//.hostPort(8100)
			.build());

		/* Add envoy container to taskdefinition */
		Map<String, String> envoyMap  = new HashMap<String, String>() {{
			put("APPMESH_VIRTUAL_NODE_NAME", "mesh/moody-mesh/virtualNode/hello-vn");
			put("ENABLE_ENVOY_XRAY_TRACING", "trace");
			put("ENVOY_LOG_LEVEL", "1");
			}};

		ContainerDefinition ContainerDefinition_envoy = appMeshProxy_hello.addContainer(
			"envoy", ContainerDefinitionOptions.builder()
				.image(ContainerImage.fromEcrRepository(Repository.fromRepositoryArn(
					this, "envoy", "arn:aws:ecr:us-west-2:840364872350:repository/aws-appmesh-envoy"),"v1.12.1.0-prod"))
				.logging(logDriverHello)
				.healthCheck(HealthCheck.builder()
					.command(Arrays.asList("curl -s http://localhost:9901/server_info | grep state | grep -q LIVE"))
					.interval(Duration.seconds(5))
					.timeout(Duration.seconds(2))
					.startPeriod(Duration.seconds(10))
					.retries(3)
					.build())
				.environment(envoyMap)
				.user("1337")
				.build());

		/* Add x-ray deamon to wegood task definition to capture analytics on the service */
		ContainerDefinition containerDef_helloxray = appMeshProxy_hello.addContainer("xray-daemon",
			ContainerDefinitionOptions.builder()
				.image(ContainerImage.fromRegistry("amazon/aws-xray-daemon:1"))
				.logging(logDriverHello)
				.build());

		containerDef_helloxray.addPortMappings(PortMapping.builder()
			.containerPort(2000)
			.hostPort(2000)
			.protocol(Protocol.UDP)
			.build());

		/* Create a load-balanced Fargate service and make it public */
		ApplicationLoadBalancedFargateService applicationLoadBalancedFargateService_hello =
			ApplicationLoadBalancedFargateService.Builder.create(this, "hello")
			.serviceName("hello")
			.cluster(cluster)           // Required
			.cpu(512)                   // Default is 256
			.desiredCount(2)            // Default is 1
			.cloudMapOptions(CloudMapOptions.builder()
				.cloudMapNamespace(privateDnsNamespace)
				.name("hello")
				.build())
			.memoryLimitMiB(1024)       // Default is 512
			.publicLoadBalancer(true)   // Default is false
			.taskDefinition(appMeshProxy_hello)
			.build();

		/* Set healthchecks for Target Group supporting hello service */
		ApplicationTargetGroup applicationHelloTargetGroup =
			applicationLoadBalancedFargateService_hello.getTargetGroup();
		applicationHelloTargetGroup.configureHealthCheck(
			software.amazon.awscdk.services.elasticloadbalancingv2.HealthCheck.builder()
			.port("4567")
			//.port("8100")
			.path("/")
			.interval(Duration.seconds(120))
			.timeout(Duration.seconds(100))
			.build());

		/* Add ECS Task Execution policy to task Definition execution role */
		applicationLoadBalancedFargateService_hello.getTaskDefinition()
			.addToExecutionRolePolicy(PolicyStatement.Builder.create()
				.actions(taskExecutionPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add AWS read only access to Amazon SSM policy to task Definition execution role */
		applicationLoadBalancedFargateService_hello.getTaskDefinition()
			.addToExecutionRolePolicy(PolicyStatement.Builder.create()
				.actions(ssmPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add AWS X-Ray Daemon to relay raw trace segments data to the service's API
		and retrieve sampling data (rules, targets, etc.) to be used by the X-Ray SDK
		to Amazon SSM policy to task Definition execution role
		*/
		applicationLoadBalancedFargateService_hello.getTaskDefinition()
			.addToExecutionRolePolicy(PolicyStatement.Builder.create()
				.actions(xrayDeamonPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add ECS Task Execution policy to task Definition execution role */
		applicationLoadBalancedFargateService_hello.getTaskDefinition()
			.addToTaskRolePolicy(PolicyStatement.Builder.create()
				.actions(taskExecutionPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add AWS read only access to Amazon SSM policy to task Definition execution role */
		applicationLoadBalancedFargateService_hello.getTaskDefinition()
			.addToTaskRolePolicy(PolicyStatement.Builder.create()
				.actions(ssmPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());

		/* Add AWS X-Ray Daemon to relay raw trace segments data to the service's API
		and retrieve sampling data (rules, targets, etc.) to be used by the X-Ray SDK
		to Amazon SSM policy to task Definition execution role
		*/
		applicationLoadBalancedFargateService_hello.getTaskDefinition()
			.addToTaskRolePolicy(PolicyStatement.Builder.create()
				.actions(xrayDeamonPolicy)
				.effect(Effect.ALLOW)
				.resources(Arrays.asList("*"))
				.build());


		/*
		Create App Mesh for wegood and hello microservices ---------------------
		*/

		/* Define the service mesh: */
		Mesh moodyMesh = Mesh.Builder.create(this, "moody-mesh")
			.meshName("moody-mesh")
			.egressFilter(MeshFilterType.ALLOW_ALL)
			.build();

		/* Define backend virtual service wegood */

		VirtualNode virtualNode_wegood = VirtualNode.Builder.create(this, "wegood-vn")
			.mesh(moodyMesh)
			.virtualNodeName("wegood-vn")
			.dnsHostName("wegood.moody.local")
			.listener(VirtualNodeListener.builder()
				.portMapping(software.amazon.awscdk.services.appmesh.PortMapping.builder()
					.port(4567)
					//.port(8000)
					.protocol(software.amazon.awscdk.services.appmesh.Protocol.HTTP)
					.build())
				.build())
			.build();

		VirtualService virtualService_wegood = VirtualService.Builder.create(this,"wegood-vs")
			.virtualServiceName("wegood.moody.local")
			.virtualNode(virtualNode_wegood)
			.mesh(moodyMesh)
			.build();

		/* Define backend virtual service wegood */

		java.util.List<IVirtualService> backendList = new ArrayList<IVirtualService>();
		backendList.add(virtualService_wegood);

		VirtualNode virtualNode_hello = VirtualNode.Builder.create(this, "hello-vn")
			.mesh(moodyMesh)
			.virtualNodeName("hello-vn")
			.backends(backendList)
			.dnsHostName("hello")
			//.dnsHostName("hello.moody.local")
			.listener(VirtualNodeListener.builder()
				.portMapping(software.amazon.awscdk.services.appmesh.PortMapping.builder()
					.port(4567)
					//.port(8100)
					.protocol(software.amazon.awscdk.services.appmesh.Protocol.HTTP)
					.build())
				.build())
			.build();

		VirtualService virtualService_hello = VirtualService.Builder.create(this,"hello-vs")
			.virtualServiceName("hello.moody.local")
			.virtualNode(virtualNode_hello)
			.mesh(moodyMesh)
			.build();

		/* Add Security group to access wegood from hello via service discovery */

		List<ISecurityGroup> helloServiceSG =  applicationLoadBalancedFargateService_hello
			.getService().getConnections().getSecurityGroups();
		//System.out.println("Hello Security Group count:" + helloServiceSG.size());

		List<ISecurityGroup> wegoodServiceSG = applicationLoadBalancedFargateService_wegood
			.getService().getConnections().getSecurityGroups();

		/* This is a local variable while to traverse the security groups to allow
		connections between them.
		*/
		wegoodServiceSG.forEach((wegoodSG) -> {
			helloServiceSG.forEach((helloSG) -> {
				ISecurityGroup helloSecurityGroup = SecurityGroup.fromSecurityGroupId(
					this, "helloSG", helloSG.getSecurityGroupId());

				/* Default is not allow */
				//wegoodSG.getConnections().addSecurityGroup(helloSecurityGroup);
				wegoodSG.getConnections().allowFrom(helloSecurityGroup.getConnections(),
					Port.tcp(4567),
					//Port.tcp(8000),
					"Hello to WeGood service discovery SG");
			});
		});
	}
}
