/* Error message:

2:38:18 PM | CREATE_FAILED        | AWS::AppMesh::VirtualNode                  | moodymeshidwegoodvnidD718E30C
Only one type of Service Discovery Configuration may be set. (Service: AWSAppMesh; Status Code: 400; Error Code: BadRequestException; Request ID: f5dba1c5-
fc69-40bd-8b63-c108e5c74bee; Proxy: null)

	new VirtualNode (/private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-kernel-7dBhRL/node_modules/@aws-cdk/aws-appmesh/lib/virtual-node.js:100:22)
	\_ Mesh.addVirtualNode (/private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-kernel-7dBhRL/node_modules/@aws-cdk/aws-appmesh/lib/mesh.js:51:16)
	\_ /private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-java-runtime17856180552964638777/jsii-runtime.js:7665:51
	\_ Kernel._wrapSandboxCode (/private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-java-runtime17856180552964638777/jsii-runtime.js:8298:20)
	\_ /private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-java-runtime17856180552964638777/jsii-runtime.js:7665:25
	\_ Kernel._ensureSync (/private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-java-runtime17856180552964638777/jsii-runtime.js:8274:20)
	\_ Kernel.invoke (/private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-java-runtime17856180552964638777/jsii-runtime.js:7664:26)
	\_ KernelHost.processRequest (/private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-java-runtime17856180552964638777/jsii-runtime.js:7372:28)
	\_ KernelHost.run (/private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-java-runtime17856180552964638777/jsii-runtime.js:7312:14)
	\_ Immediate._onImmediate (/private/var/folders/ky/lk68r_mx5pv85zrbf24z5nfc0000gn/T/jsii-java-runtime17856180552964638777/jsii-runtime.js:7315:37)
	\_ processImmediate (internal/timers.js:456:21)
*/

package getmymoodyresponse.cdk;

//import hello.cdk.Hello;
//import wegood.cdk.WeGood;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.Duration;
import software.amazon.awscdk.core.RemovalPolicy;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.core.StackProps;
import software.amazon.awscdk.services.ec2.Vpc;
import software.amazon.awscdk.services.ec2.Port;
import software.amazon.awscdk.services.ec2.SecurityGroup;
import software.amazon.awscdk.services.ec2.ISecurityGroup;
import software.amazon.awscdk.services.ecs.Protocol;
import software.amazon.awscdk.services.ecr.Repository;
import software.amazon.awscdk.services.ecs.Cluster;
import software.amazon.awscdk.services.ecs.ScalableTaskCount;
import software.amazon.awscdk.services.ecs.FargateService;
import software.amazon.awscdk.services.ecs.ContainerImage;
import software.amazon.awscdk.services.ecs.CloudMapOptions;
import software.amazon.awscdk.services.ecs.Secret;
import software.amazon.awscdk.services.ecs.HealthCheck;
import software.amazon.awscdk.services.ecs.ContainerDefinitionOptions;
import software.amazon.awscdk.services.ecs.PortMapping;
import software.amazon.awscdk.services.ecs.ContainerDefinitionOptions;
import software.amazon.awscdk.services.ecs.RequestCountScalingProps;
import software.amazon.awscdk.services.ecs.ContainerDefinition;
import software.amazon.awscdk.services.ecs.AppMeshProxyConfiguration;
import software.amazon.awscdk.services.ecs.FargateTaskDefinition;
import software.amazon.awscdk.services.ecs.AppMeshProxyConfigurationProps;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedTaskImageOptions;
import software.amazon.awscdk.services.elasticloadbalancingv2.ApplicationTargetGroup;
import software.amazon.awscdk.services.iam.AnyPrincipal;
import software.amazon.awscdk.services.iam.Effect;
import software.amazon.awscdk.services.iam.ManagedPolicy;
import software.amazon.awscdk.services.iam.PolicyStatement;
import software.amazon.awscdk.services.iam.Role;
import software.amazon.awscdk.services.iam.ServicePrincipal;
import software.amazon.awscdk.services.logs.LogGroup;
import software.amazon.awscdk.services.servicediscovery.PrivateDnsNamespace;
import software.amazon.awscdk.services.ssm.StringParameter;
import software.amazon.awscdk.services.appmesh.Mesh;
import software.amazon.awscdk.services.appmesh.MeshFilterType;
import software.amazon.awscdk.services.appmesh.VirtualNode;
import software.amazon.awscdk.services.appmesh.VirtualNodeListener;
import software.amazon.awscdk.services.appmesh.VirtualService;
import software.amazon.awscdk.services.cloudwatch.Metric;
import software.amazon.awscdk.services.applicationautoscaling.EnableScalingProps;
import software.amazon.awscdk.services.applicationautoscaling.TargetTrackingScalingPolicy;
import software.amazon.awscdk.services.appmesh.IVirtualService;

import software.amazon.awscdk.services.appmesh.VirtualNodeBaseProps;
import software.amazon.awscdk.services.appmesh.VirtualServiceBaseProps;
import software.amazon.awscdk.services.servicediscovery.Service;

public class TestStack2 extends Stack {

	private Hello hello;
	private WeGood wegood;

    public TestStack2(final Construct scope, final String id) {
        this(scope, id, null, "wegood.stub.WeGoodProxyFactory");
    }

    public TestStack2(final Construct scope, final String id, final StackProps props,
		String weGoodFactoryClassName) {

        super(scope, id, props);

		/* Create VPC with a max of two availability zones: */
		Vpc vpc = Vpc.Builder.create(this, "moody-vpc")
			.maxAzs(2)  // Default is all AZs in region
			.build();

		String namespaceName = "moody.local";

		PrivateDnsNamespace privateDnsNamespace =
			PrivateDnsNamespace.Builder.create(this, namespaceName)
				.name(namespaceName).vpc(vpc).build();

		/* Create ECS fargate-backed cluster within the VPC created above: */
		Cluster cluster = Cluster.Builder.create(this, "moody-cluster")
			.vpc(vpc).clusterName("moody-cluster")
			.build();

		//String weGoodServiceURL = "http://" + "wegood:80";
		//String weGoodServiceURL = "http://" + "wegood" + "." + namespaceName + ":4567";
		String weGoodServiceURL = "http://" + "wegood" + "." + namespaceName + ":80";
			// YourServiceDiscoveryServiceName.YourServiceDiscoveryNamespace
		this.hello = new Hello(this, cluster, "hello", props, "hello", "wegood.stub.WeGoodProxyFactory",
			weGoodServiceURL, privateDnsNamespace);
		this.wegood = new WeGood(this, cluster, "wegood", props, "wegood", privateDnsNamespace);


		/*
		Define the service mesh: -----------------------------------------------
		Ref: https://docs.aws.amazon.com/cdk/api/latest/docs/aws-appmesh-readme.html
		*/

		Service wegoodService = privateDnsNamespace.createService("wegood");
		Service helloService = privateDnsNamespace.createService("hello");

		/* Define the service mesh: */
		Mesh mesh = Mesh.Builder.create(this, "moody-mesh-id")
			.meshName("moody-mesh")
			.egressFilter(MeshFilterType.ALLOW_ALL)
			.build();

		/* Define the WeGood virtual node: */
		VirtualNode virtualNode_wegood = mesh.addVirtualNode("wegood-vn-id",
			VirtualNodeBaseProps.builder()
			.virtualNodeName("wegood-vn")
			.dnsHostName("wegood.moody.local")
			.cloudMapService(wegoodService)
			.listener(VirtualNodeListener.builder()
				.portMapping(software.amazon.awscdk.services.appmesh.PortMapping.builder()
					.port(4567)
					.protocol(software.amazon.awscdk.services.appmesh.Protocol.HTTP)
					.build())
				.build())
			.build());

		/* Define the WeGood virtual service: */
		VirtualService virtualService_wegood = mesh.addVirtualService("wegood-vs-id",
			VirtualServiceBaseProps.builder()
			.virtualNode(virtualNode_wegood)
			.virtualServiceName("wegood-vs")
			.build());

		/* Define the Hello virtual node: */
		java.util.List<IVirtualService> backendList = new ArrayList<IVirtualService>();
		backendList.add(virtualService_wegood);
		VirtualNode virtualNode_hello = mesh.addVirtualNode("hello-vn-id",
			VirtualNodeBaseProps.builder()
			.backends(backendList)
			.virtualNodeName("hello-vn")
			.dnsHostName("hello.moody.local")
			.cloudMapService(helloService)
			.listener(VirtualNodeListener.builder()
				.portMapping(software.amazon.awscdk.services.appmesh.PortMapping.builder()
					.port(4567)
					//.port(8100)
					.protocol(software.amazon.awscdk.services.appmesh.Protocol.HTTP)
					.build())
				.build())
			.build());

		/* Define the Hello virtual service: */
		VirtualService virtualService_hello = mesh.addVirtualService("hello-vs-id",
			VirtualServiceBaseProps.builder()
			.virtualNode(virtualNode_hello)
			.virtualServiceName("hello-vs")
			.build());

		/* Add allowed route to each security group, to access wegood from hello
		via service discovery */

		List<ISecurityGroup> helloServiceSG =  this.hello.getALBFS()
			.getService().getConnections().getSecurityGroups();

		List<ISecurityGroup> wegoodServiceSG = this.wegood.getALBFS()
			.getService().getConnections().getSecurityGroups();

		wegoodServiceSG.forEach((wegoodSG) -> {
			helloServiceSG.forEach((helloSG) -> {
				ISecurityGroup helloSecurityGroup = SecurityGroup.fromSecurityGroupId(
					this, "helloSG", helloSG.getSecurityGroupId());

				/* Default is not allow */
				wegoodSG.getConnections().allowFrom(helloSecurityGroup.getConnections(),
					Port.tcp(4567),
					"Hello to WeGood service discovery SG");
			});
		});
	}
}

class Hello {

	private ApplicationLoadBalancedFargateService fgs;

    public Hello(final Stack stack, final Cluster cluster, final String id,
		final StackProps props, String serviceName, String weGoodFactoryClassName,
		String weGoodServiceURL, PrivateDnsNamespace privateDnsNamespace) {

		/* Pass name of the WeGood stub factory class to the container env: */
		Map containerEnvMap = new HashMap();
		containerEnvMap.put("WEGOOD_FACTORY_CLASS_NAME", weGoodFactoryClassName);
		containerEnvMap.put("WEGOOD_SERVICE_URL", weGoodServiceURL);

		/* Create a load-balanced Fargate service for the hello microservice and
			make it public: */
		this.fgs = ApplicationLoadBalancedFargateService.Builder.create(stack, id)
			.serviceName(serviceName)
			.cluster(cluster)           // Required
			.memoryLimitMiB(2048)       // Default is 512
			.cpu(512)                   // Default is 256
			.desiredCount(1)            // Default is 1

			.cloudMapOptions(CloudMapOptions.builder() // auto discovery mode
				.cloudMapNamespace(privateDnsNamespace)
				.name(serviceName)
				.build())

			.taskImageOptions(
				ApplicationLoadBalancedTaskImageOptions.builder()
				.image(ContainerImage.fromRegistry("cliffberg/hello"))
				.containerPort(4567)
				.environment(containerEnvMap)
				.enableLogging(true)
				.build())
			.publicLoadBalancer(true)   // Default is false
			.build();
    }

	public ApplicationLoadBalancedFargateService getALBFS() {
		return this.fgs;
	}
}

class WeGood {

	private ApplicationLoadBalancedFargateService fgs;

    public WeGood(final Stack stack, final Cluster cluster, final String id,
		final StackProps props, String serviceName, PrivateDnsNamespace privateDnsNamespace) {

		/* Pass name of the WeGood stub factory class to the container env: */
		Map containerEnvMap = new HashMap();
		containerEnvMap.put("WEGOOD_SERVICE_URL", "http://" + serviceName);

		/* Create a load-balanced Fargate service for the wegood microservice and
			make it public: */
		this.fgs = ApplicationLoadBalancedFargateService.Builder.create(stack, id)
			.serviceName(serviceName)
			//.domainZone(domainZone)
			//.domainName(serviceName + "." + domainZone)
			.cluster(cluster)           // Required
			.memoryLimitMiB(2048)       // Default is 512
			.cpu(512)                   // Default is 256
			.desiredCount(1)            // Default is 1

			.cloudMapOptions(CloudMapOptions.builder() // auto discovery mode
				.cloudMapNamespace(privateDnsNamespace)
				.name(serviceName)
				.build())

			.taskImageOptions(
				ApplicationLoadBalancedTaskImageOptions.builder()
				.image(ContainerImage.fromRegistry("cliffberg/wegood"))
				.containerPort(4567)
				.environment(containerEnvMap)
				.build())
			.publicLoadBalancer(true)   // Default is false
			.build();
    }

	public ApplicationLoadBalancedFargateService getALBFS() {
		return this.fgs;
	}
}
